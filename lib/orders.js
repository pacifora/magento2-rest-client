var util = require('util');
module.exports = function (restClient) {
    var module = {};

    module.list = function(searchCriteria) {
      const query = 'searchCriteria' + searchCriteria;
      const endpointUrl = util.format('/orders?%s', query);
      return restClient.get(endpointUrl);
    }

    return module;
}
